Exercise 2.1
------------

Implementation is given [here](scripts/Chapter2/exercise1.scm).

Exercise 2.2
------------

Implementation is given [here](scripts/Chapter2/exercise2.scm).

Exercise 2.3
------------

Implementation is given [here](scripts/Chapter2/exercise3.scm).

Exercise 2.4
------------

Let us examine what happens when `(car (cons x y))` is evaluated.
```
(car (cons x y))
(car (lambda (m) (m x y)))
((lambda (m) (m x y)) (lambda (p q) p))
((lambda (p q) p) x y)
x
```
So it does indeed work.

The corresponding definition of `cdr` is simply
```
(define (cdr z)
  (z (lambda (p q) q)))
```

Exercise 2.5
------------

The fact that we can represent a pair of non-negative numbers $`(a, b)`$ as the quantity $`2^a 3^b`$ is a direct consquence of the fundamental theorem of arithmetic.
As numbers have unique prime factorisations, we can always retrieve $`a`$ and $`b`$ by factoring the number that is representing the pair.
Simple definitions of `cons`, `car`, and `cdr` are given [here](scripts/Chapter2/exercise5.scm).

Exercise 2.6
------------

Implementation is given [here](scripts/Chapter2/exercise6.scm).
I have taken the liberty of adding a few more Church numeral functions in for fun

Exercise 2.7
------------

Implementation is given [here](scripts/Chapter2/exercise7-15.scm).

Exercise 2.8
------------

Implementation is given [here](scripts/Chapter2/exercise7-15.scm).

Exercise 2.9
------------

Let $`a = (a_1, a_2)`$ and $` b = (b_1, b_2)`$ be intervals. Further, let $`w(a)`$ be the *width* of $`a`$.
We note that the widths of these 2 intervals are
```math
\begin{aligned}
w(a) &= \frac{a_2 - a_1}{2} \\
w(b) &= \frac{b_2 - b_1}{2}.
\end{aligned}
```
Using addition as defined by Alyssa, we get that the sum of these intervals can be calculated simply as.
```math
a + b = (a_1 + b_1, a_2 + b_2).
```
We now calculate the width of this interval
```math
\begin{aligned}
w(a + b) &= \frac{(a_2 + b_2) - (a_1 + b_1)}{2}         \\
         &= \frac{(a_2 - a_1) + (b_2 - b_1)}{2}         \\
         &= \frac{a_1 + a_2}{2} + \frac{b_1 + b_2}{2}   \\
         &= w(a) + w(b).
\end{aligned}
```

This also applies to subtraction, since subtraction can be defined as adding the negative.
The width of the negative interval is unchanged, so the width of the sum is the same as the width of the difference.

The widths of products and quotients however, cannot be calculated using only the widths of the operands.
For example, consider $`(1, 2) \times (1, 2) = (1, 4)`$. The operands both have a width of 1 and the result has a width of 3.
Now consider $`(2, 3) \times (2, 3) = (4, 9)`$. Here, the operands still both have a width of 1, but the result has a width of 5.

As division is just multiplying by the reciprocal, this also applies to division.

Exercise 2.10
-------------

Implementation is given [here](scripts/Chapter2/exercise7-15.scm).

Exercise 2.11
-------------

Implementation is given [here](scripts/Chapter2/exercise7-15.scm).

Exercise 2.12
-------------

Implementation is given [here](scripts/Chapter2/exercise7-15.scm).

Exercise 2.13
-------------

Suppose that we have two intervals defined in terms of percentage tolerances
```math
\begin{aligned}
a &= \left(c_a - c_a p_a, c_a + c_a p_a \right) &= c_a \left(1 - p_a, 1 + p_a \right) \\
b &= \left(c_b - c_b p_b, c_b + c_b p_b \right) &= c_b \left(1 - p_b, 1 + p_b \right).
\end{aligned}
```

We assume that both intervals are entirely positive and that the percentage tolerances are sufficently small that quadratic terms of the tollerances are negligible.

Then we compute the product as
```math
\begin{aligned}
ab &= c_a c_b \left( (1 - p_a) (1 - p_b) , (1 + p_a) (1 + p_b) \right)          \\
   &= c_a c_b \left( 1 - p_a - p_b + p_a p_b, 1 + p_a + p_b + p_a p_b \right)   \\
   &= c_a c_b \left( 1 - (p_a + p_b), 1 + (p_a + p_b) \right).
\end{aligned}
```
We used the fact that $`p_a p_b`$ is quadratic and thus negligible in the last step.
Thus the percentage tolerance of the product is simply the sum of the percentage tolerances.
That is $`p_{ab} = p_a + p_b`$.

Exercise 2.14
-------------

The easiest way to demonstrate that Lem is correct is to run the program.
```
(define R1 (make-center-percent 10 10))
(define R2 (make-center-percent 2.2 5))

(print-interval (par1 R1 R2))
-> (1.413223140495868, 2.291253381424707)

(print-interval (par2 R1 R2))
-> (1.6961226330027055, 1.909090909090909)
```
Thus we know there is a problem, what follows is a discussion of why there is a problem.

It may seem counter-intuitive, but I will define $`A`$ and $`B`$ to be 2 different objects with the same value.
We let
```math
\begin{aligned}
A := 100 \pm 10\% = (90, 110) \\
B := 100 \pm 10\% = (90, 110).
\end{aligned}
```

The value of $`A / B`$ is easy to predict, we should expect an interval centered around 1, with percentage tolerance near 20%.
Here is the result of calculating it with the system:
```
(define A (make-center-percent 100 10))
(define B (make-center-percent 100 10))
(define A/B (div-interval A B))

(print-interval A/B)
-> (0.8181818181818181, 1.2222222222222223)

(center A/B)
-> 1.0202020202020203

(percent A/B)
-> 19.80198019801981
```
This is the expected result. The problem arises if we calculate $`A / A`$.
Because this is algebraicly equivalent to simply $`1`$ we should expect the interval $`(1, 1)`$ exactly,
but of course the system has no way of distinguishing this from the previous problem and returns the same values.
```
(define A (make-center-percent 100 10))
(define A/A (div-interval A A))

(print-interval A/A)
-> (0.8181818181818181, 1.2222222222222223)

(center A/A)
-> 1.0202020202020203

(percent A/A)
-> 19.80198019801981
```

Exercise 2.15
-------------

Eva is indeed correct.

If we consider these intervals as mathematical sets, we can view them as the set of all possible values that $`A`$ can assume.
Then the quotient of two intervals $`A`$ and $`B`$ is the set of all values of the form $`a / b`$ where $`a \in A`$ and $`b \in B`$.

However if $`A`$ and $`B`$ were the *same* interval, this would not be the correct answer.
An interval has a *single* "correct" value, we cannot use two different values from the same interval in the same calculation.
Thus to calculate $`A / A`$ correctly, we would form the set of all values of the form $`a / a`$ where $`a \in A`$.
That is to say that if a variable appears twice in an expression, we must always choose the same potential value for each appearance of the variable.

Any expression where each variable appears exactly once is the only way to fulfill this condition with the current system.

Thus, yes, `par2` is indeed a "better" program than `par1`.

Exercise 2.16
-------------

As we observed in Exercise 2.15, The problem lies in the fact that if a value appears more than once in an expression, we should choose values for it consistently.

There is a very tempting easy (but ultimately insufficent) solution to this problem.
It lies in the fact that addition and multiplication (for intervals that do not include 0) produce correct results even if a variable is repeated.
The real problem is division and subtraction.
On calculating a negative or a reciprocal to an interval, the order of the bounds is changed.
That is to say, if we want to pick the point that corresponds to the lower bound of $`A`$ we must choose the *upper* bound of $`-A`$.
The tempting easy solution is to simply swap the lower and upper bounds if we do a subtraction by an expression that includes itself.

This however is not always possible. Suppose we wish to calculate  $`(A B) / (A C)`$.
We first would calculate the two products that result in the numerator and denominator, and then perform the division.
We notice that both terms contain a variable $`A`$ and so we must reverse the bounds of one,
but we can no longer do that as it would also reverse the bounds of $`C`$ resulting in an incorrect answer.

To do this properly, we must keep track of all possible values an interval could have depending on which values we choose for which variables
and then combine the expressions in a way that maintains the consistency.

I have implemented this [here](scripts/Chapter2/exercise16.scm) by using a tree to keep track of all possible choices of endpoints for all constituent intervals.
Here is a quick sample output using `par1` and `par2`.
```
(define R1 (make-center-percent 10 10))
(define R2 (make-center-percent 2.2 5))

(print-interval (par1 R1 R2))
-> (1.6961226330027053, 1.909090909090909)

(print-interval (par2 R1 R2))
-> (1.6961226330027055, 1.909090909090909)
```

Exercise 2.17
-------------

Implementation is given [here](scripts/Chapter2/exercise17.scm).

Exercise 2.18
-------------

Implementation is given [here](scripts/Chapter2/exercise18.scm).

Exercise 2.19
-------------

Implementation is given [here](scripts/Chapter2/exercise19.scm).
Coin order does not matter as this search is exhaustive. That is, it checks every possible combination of coins.

Exercise 2.20
-------------

Implementation is given [here](scripts/Chapter2/exercise20.scm).

Exercise 2.21
-------------

Implementation is given [here](scripts/Chapter2/exercise21.scm).

Exercise 2.22
-------------

If we follow through the steps we get:
```
(square-list '(1 2 3 4))
(iter '(1 2 3 4) '()
(iter (cdr '(1 2 3 4))
      (cons (square (car '(1 2 3 4))
                    '())))
(iter '(2 3 4)
      (cons 1 '()))
(iter '(2 3 4) '(1))
```
We see that the first thing to be `cons`ed to the empty list is the first element of the list.

Louis' second attempt produces a left-recursive list rather than the normal right-recursive list.
That is to say that if one were to take the `car` of the result, one would get a left-recursive list containing all the elements but the last
and if one were to take the `cdr` of the result, one would get the value of the last thing in the list.
This is quite different to the behaviour of regular right-recursive lists.

Exercise 2.23
-------------

Implementation is given [here](scripts/Chapter2/exercise23.scm).

Exercise 2.24
-------------

Evaluating the expression with the interpreter gives:
```
(list 1 (list 2 (list 3 4)))
-> (1 (2 (3 4)))
```
The corresponding box-and-pointer structure is:

![box-and-pointer structure](images/2-24_box-pointer.png)

The interpretation as a tree is:

![tree interpretation](images/2-24_tree.png)

Exercise 2.25
-------------

The implementation of scheme I am using (guile) has abreviated forms of nested calls `car` and `cdr` up to 4 calls deep.
Using these for my sanity we get:
```
(car (cdaddr '(1 3 (5 7) 9) ))
-> 7

(caar '((7)) )
-> 7

(cadadr (cadadr (cadadr '(1 (2 (3 (4 (5 (6 7)))))) )))
-> 7
```

Exercise 2.26
-------------

The results of the given expressions are:
```
(define x (list 1 2 3))
(define y (list 4 5 6))

(append x y)
-> (1 2 3 4 5 6)

(cons x y)
-> ((1 2 3) 4 5 6)

(list x y)
-> ((1 2 3) (4 5 6))
```

Exercise 2.27
-------------

Implementation is given [here](scripts/Chapter2/exercise27.scm).

Exercise 2.28
-------------

Implementation is given [here](scripts/Chapter2/exercise28.scm).

Exercise 2.29
-------------

#### a
Implementation is given [here](scripts/Chapter2/exercise29.scm).

#### b
Implementation is given [here](scripts/Chapter2/exercise29.scm).

#### c
Implementation is given [here](scripts/Chapter2/exercise29.scm).

#### d
All we would need to change is the selectors `left-branch`, `right-branch`, `branch-length`, and `branch-struct`.

Exercise 2.30
-------------

Implementation is given [here](scripts/Chapter2/exercise30.scm).

Exercise 2.31
-------------

Implementation is given [here](scripts/Chapter2/exercise31.scm).

Exercise 2.32
-------------

Implementation is given [here](scripts/Chapter2/exercise32.scm).

To see how it works, let $`A`$ be a set and $`a \in A`$ be some element of $`A`$.
If we want to find all the subsets of $`A`$ we can split the problem into two parts, all the subsets that contain $`a`$ and all those that do not.

We can find all the subsets that do not contain $`a`$ with a recursive strategy, We remove $`a`$ and apply this procedure to the new set.
The recursion terminates at the empty set, whose only subset is the empty set itself.

The subsets that do contain $`a`$ can be found by simply appending an $`a`$ to all the subsets that do not contain $`a`$.

This is the basis of how this program works, for a set `s` we find all the subsets that do not contain `(car s)`, that is to say, all the subsets of `(cdr s)`.
We then take this list and append it to the list of subsets that do contain `(car s)`, we obtain this by appending `(car s)` to each subset using `map`.

Exercise 2.33
-------------

Implementation is given [here](scripts/Chapter2/exercise33.scm).

Exercise 2.34
-------------

Implementation is given [here](scripts/Chapter2/exercise34.scm).

Exercise 2.35
-------------

Implementation is given [here](scripts/Chapter2/exercise35.scm).

Exercise 2.36
-------------

Implementation is given [here](scripts/Chapter2/exercise36.scm).

Exercise 2.37
-------------

Implementation is given [here](scripts/Chapter2/exercise37.scm).

Exercise 2.38
-------------

The result of the given expressions is:
```
(fold-right / 1 (list 1 2 3))
-> 3/2

(fold-left / 1 (list 1 2 3))
-> 1/6

(fold-right list '() (list 1 2 3))
-> (1 (2 (3 ())))

(fold-left list '() (list 1 2 3))
-> (((() 1) 2) 3)
```

The condition for an operation `op` that guarentees that `fold-right` and `fold-left` produce the same values is associativity.
That is, for any `a`, `b` and `c` we should have that `(op (op a b) c)` gives the same value as `(op a (op b c))`.

Exercise 2.39
-------------

Implementation is given [here](scripts/Chapter2/exercise39.scm).

Exercise 2.40
-------------

Implementaiton is given [here](scripts/Chapter2/exercise40.scm).

Exercise 2.41
-------------

Implementaiton is given [here](scripts/Chapter2/exercise41.scm).

Exercise 2.42
-------------

Implementation is given [here](scripts/Chapter2/exercise42.scm).

Exercise 2.43
-------------

In swapping the order of the nested mappings, Louis has instructed the compiler to re-calculate `(queen-cols (- k 1))` for each potential new position.
This means that every call to `(queen-cols k)` will call `(queen-cols (- k 1))` repeatedly `board-size` times.
Thus for the 8 queens problem, we expect `(queen-cols 1)` to take 8 times as long, and thus `(queen-cols 2)` to take $`8 \times 8`$ as long and so on.
This results in us expecting `(queens 8)` to take $`8^8`$ times as long to complete as a correct implementation.

My computer can run it in around 20 ms, thus we would expect Louis Reasoner's system to take around 3 days 21 hours to complete.

Exercises 2.44 ~ 2.52
---------------------

These exercises concern the use of a graphical picture language.
I can find no convenient way to do simple drawings in guile, so I will leave these exercises untill I can be bothered making my own module for this task.

Exercise 2.53
-------------

The results of the given expressions are:
```
(list 'a 'b 'c)
-> (a b c)

(list (list 'george))
-> ((george))

(cdr '((x1 x2) (y1 y2)))
-> ((y1 y2))

(cadr '((x1 x2) (y1 y2)))
-> (y1 y2)

(pair? (car '(a short list)))
-> #f

(memq 'red '((red shoes) (blue socks)))
-> #f

(memq 'red '(red shoes blue socks))
-> (red shoes blue socks)
```

Exercise 2.54
-------------

Implementation is given [here](scripts/Chapter2/exercise54.scm).

Exercise 2.55
-------------

As quoting with `'` is equivalent to calling the function `quote`, we find that `''abracadabra` is expanded into `(quote abracadabra)`.
This is simply a list with first element `'quote` and so this is what the interpreter prints back.

Exercise 2.56
-------------

Implementation is given [here](scripts/Chapter2/exercise56-57.scm).

Exercise 2.57
-------------

Implementation is given [here](scripts/Chapter2/exercise56-57.scm).

Exercise 2.58
-------------

#### a
Implementation is given [here](scripts/Chapter2/exercise58a.scm).

#### b
Implementation is given [here](scripts/Chapter2/exercise58b.scm).

Exercise 2.59
-------------

Implementation is given [here](scripts/Chapter2/exercise59.scm).

Exercise 2.60
-------------

Implementation is given here [here](scripts/Chapter2/exercise60.scm).

The implementations of `element-of-set?` and `intersection-set` are exactly the same, so they also have efficencies $`\Theta(n)`$ and $`\Theta(n^2)`$ respectively.
The operation `adjoin-set` is simply a call to `cons` and as such has efficiency $`\Theta(1)`$ compared to the $`\Theta(n^2)`$ for the non-duplicate representation.
The operation `union-set` is simply a call to `append` and as such has efficiency $`\Theta(n)`$ compared to the $`\Theta(n^2)`$ for the non-duplicate representation.

The efficiency of all the procedures is better than the non-duplicate representations.
However, allowing duplicates could allow $`n`$ to grow significantly larger than what it would be for the non-duplicate representation.
So this representation is preferable in cases where the elements you are adding to the set never repeat, or are very unlikely to repeat.
Also in situations where there are many cases where we want to add elements, and few cases where we want to query elements this representation may be faster.

Exercise 2.61
-------------

Implementation is given here [here](scripts/Chapter2/exercise61.scm).

Exercise 2.62
-------------

Implementation is given here [here](scripts/Chapter2/exercise62.scm).

Exercise 2.63
-------------

#### a
Both procedures will output the same list for every tree.
Both work by `cons`ing on each node starting from the right most leaf and continuing toward the left.
The only difference is that `tree->list-1` will find the list for each sub-tree independantly and `append` them together,
whereas `tree->list-2` will just create a single list for the whole tree, `cons`ing everything in order.

Both procedures produce the list
```
(1 3 5 7 9 11)
```
for all the trees in Figure 2.16.

#### b
Both procedures are $`\Theta(n)`$, this is clear in the case of `tree->list-2` as `cons` is called exactly once for each node in the tree.
However for `tree->list-1` we have an additional call to `append` when joining the lists for two subtrees.
This does not change the overall efficency however, as `append` is itself a $`\Theta(n)`$ operation.

Exercise 2.64
-------------

#### a
Essentially, the procedure chooses an element in the center of the list `this-entry`.
For every element in the list less than this value, it creates balanced trees recursively for the set of values lower than `this-entry` and the set of value higher than `this-entry`.
These trees are `left-tree` and `right-tree` respectively.
Then the procedure returns a new tree with `this-entry` as the value and `left-tree` and `right-tree` as the left and right branches.

Of course, the way lists are defined in Scheme, you cannot easily just remove the central value of a list, so instead the first thing the procedure does is construct `left-tree`.
Then it sets aside the next element as `this-tree`, any remaining elements are used to construct `right-tree`.
Finally, the new tree is constructed.

The result of constructing a tree from the given list is
```
(list->tree '(1 3 5 7 9 11))
-> (5 (1 () (3 () ())) (9 (7 () ()) (11 () ())))
```
Interpreting this as a tree gives:

![resulting tree](images/2-64_tree.png)

#### b
Notice the recursive nature of the procedure.
Each call to `partial-tree` calls itself twice on a list of half as many elements.
This suggests that the time efficency is $`\Theta(\log(n))`$.

Exercise 2.65
-------------

Implementation is given [here](scripts/Chapter2/exercise65.scm).

We define them by first converting the operands to ordered lists, performing a list union or intersection, and then converting back to a tree.
Note that as both the list union and intersection are $`\Theta(n)`$ we get that the effiency of this procedure is $`\Theta(\log(n)) + \Theta(n) + 2 \Theta(n) = \Theta(n)`$.

Exercise 2.66
-------------

Implementation is given [here](scripts/Chapter2/exercise66.scm).

Exercise 2.67
-------------

The result of using `decode` is:
```
(decode sample-message sample-tree)
-> (A D A B B C A)
```

Exercise 2.68
-------------

Implementation is given [here](scripts/Chapter2/exercise68.scm).

Exercise 2.69
-------------

Implementation is given [here](scripts/Chapter2/exercise69.scm).

Exercise 2.70
-------------

Encoding the message gives:
```
(define rock-tree
  (generate-huffman-tree
    '((A    2) (NA  16)
      (BOOM 1) (SHA  3)
      (GET  2) (YIP  9)
      (JOB  2) (WAH  1))))

(define rock
  '(GET A JOB
    SHA NA NA NA NA NA NA NA NA

    GET A JOB
    SHA NA NA NA NA NA NA NA NA

    WAH YIP YIP YIP YIP YIP YIP YIP YIP YIP
    SHA BOOM))

(encode rock rock-tree)
-> (1 1 1 1 1 1 1 0 0 1 1 1 1 0 1 1 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 0 0 1 1 1
    1 0 1 1 1 0 0 0 0 0 0 0 0 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1
    1 1 0 1 1 0 1 1)

(length (encode rock rock-tree))
-> 84
```

If we were to use a fixed length code, we would require 3 bits per character to encode 8 different characters.
Then to encode a 36 character message we would require 108 bits.
Thus the variable length code gives us a 22% improvement over the fixed length code.

Exercise 2.71
-------------

Assigning the frequencies to letters in alphabetical order with A being the least frequent letter we get this tree for $`n = 5`$:

![Huffman tree for n=5](images/2-71_5-tree.png)

For the $`n = 10`$ case we get

![Huffman tree for n=10](images/2-71_10-tree.png)

We see that we simply get a tree that is as unbalanced as possible. This requires $`n-1`$ bits for the least frequent symbol and $`1`$ bit for the most frequent symbol.

Exercise 2.72
-------------

The time it takes to find the symbol in the list is $`\Theta(n)`$ where $`n`$ is the number of leaves below the current node.

We can consider the situation described in Exercise 2.71 as the worst case scenario.
In this case, we consider the encoding of the least frequent symbol, at each step, the symbols list is reduced only by one so we have $`\Theta(n + (n - 1) + (n - 2) + ...)`$.
This sum can be evaluated as a arithmetic series as $`\Theta(n^2)`$.
In the case of the most frequent symbol, we only need to check the first node which take $`\Theta(n)`$ time.
I believe once this is averaged with the frequency of the symbols, we will get amortized $`\Theta(n)`$ time, but proving this is a pain.

Now we consider the best case situation, one where the code tree is balanced. This is the case we would expect if all symbols have the same frequency.
In this case we half the number of symbols at each step meaning the number of steps is $`\Theta(n + n/2 + n/4 + ...)`$.
Although this sum certainly terminates, we can get an upper bound for it by evaluating the infinite case as a geometric series giving an upper bound of $`O(n)`$.
We also have that $`o(n)`$ is a lower bound because the first node takes $`\Theta(n)`$ steps meaning that the overall lookup is $`\Theta(n)`$.

To conclude, the time to lookup a symbol is in the worst case $`\Theta(n^2)`$ and the best case is $`\Theta(n)`$.
I also believe that the average case is $`\Theta(n)`$.

Exercise 2.73
-------------

#### a
By using a generic deriv operation in this manner we hace made the problem of adding additional derivitave rules additive.
Instead of each programer modifying the one `deriv` procedure, programers add a package that is automatically incorporated into the generic `deriv` procedure.
Variables and numbers can't be incorporated because, although they are expressions, they do not have an operator that we can use to locate their generic procedure.
We could incorporate them into the system if we tag them as well, but this makes things more complicated than they really need to be.

#### b
Implementation is given [here](scripts/Chapter2/exercise73.scm).

#### c
I chose the exponential differentiation rule.
Implementation is given [here](scripts/Chapter2/exercise73.scm).

#### d
Although we could just change the order of the operands for the calls to `put` inside the package and everything would work,
this is probably not the best idea as changing the indexing in this way makes a system that is conceptually very different.

In a system like this, the *type* of the expression is `deriv` and the *operation* is `+` or `*`.
We should expect `(get '* 'deriv)` to be a procedure that multiplies two differentiation expressions together to produce a new differentiation expression,
that is to say that this procedure is not `deriv-product` but rather `make-product`.

Regardless of interpretation, the system ceases being additive as now we expect all functions to do with derivatives to be in the same package.

Exercise 2.74
-------------

The problem can be solved using a set of generic query procedures that can interpret the different data structures that each division uses.

#### a
An implementation of a `get-record` procedure is given [here](scripts/Chapter2/exercise74.scm).
Each division is expected to tag all of their personnel files with a type tag unique to that division.
They are also expected to provide a package with a `get-record` procedure that takes an argument of type `'name` for the employee name
and an argument of the type that their personnel files are tagged as for the personnel file.

#### b
An implementation of a `get-salary` procedure is given [here](scripts/Chapter2/exercise74.scm).
Each division is expected to tag all of their employee records with a type tag unique to that division.
They are also expected to proved a package with a `get-salary` procedure that takes an argument of the type that their employee records are taged with.

#### c
An implementation of a `find-employee` procedure is given [here](scripts/Chapter2/exercise74.scm).
This procedure simply makes use of the `get-record` procedure given above.
If a division is compliant with that procedure, it is also compliant with this one.

#### d
The new division must tag their personnel files and employee records with type tags unique to the new division.
They must also provide a package that provides query procedures `get-record` and `get-salary` as specified above.
No changes need be made to the central system with the exception of loading the package provided by the new division.

Exercise 2.75
-------------

Implementation is given [here](scripts/Chapter2/exercise75.scm).

Exercise 2.76
-------------

Nothing comes for free.
In all cases, adding a new type requires implementing all existing operations for that type and adding a new operation requires implementing that operation for all existing types.
The difference comes in how the new types are incorproated into generic procedures and how new procedures are defined for existing types
and as such, I will only consider these effects.

In the case of explicit dispatch, nothing is very fun.
To add a new type, we must modify every existing generic procedure explicitly to have a clause for this new type.
To add a new operation, we must write a new generic procedure with explicit clauses for each and every type that exists in our system.
In addition to being inconvenient, this style is not additive.
That is to say that one is forced to modify the master generic procedure for each operator and operators specific to types cannot share names.

In the case of the data directed approach, the situation improves considerably.
Now, when new types are added, the generic procedures do not need to be changed at all, new types are incorporated automatically.
To add a new operation, we have to write a new generic procedure, but that is scarecly a wrapper on the `apply-generic` procedure.
Importantly, this approach *is* additive, new types can be added without having to alter existing generic procedures.

Lastly, in the case of the message passing approach, we have a similar situation.
When new types are added, we simply define the dispatch operation for that type and we are done without altering any existing code.
To add new operations, we simply have to write a new generic procedure, which is again just a wrapper on (the message passing version of) `apply-generic`.

The style that is better for systems in which types are added frequently is difficult to decide upon.
Both the data directed and message passing approaches require minimal work but I believe the message passing approach would have slightly less friction.

It may seem that the style better for systems in which operations are added frequently is moot.
But notice that in the message passing style, to add new operaitons we are forced to modify the existing packages.
However for the data directed style, there is nothing stopping us from simply adding a package that adds new operations to existing types.

Exercise 2.77
-------------

On the first call to `magnitude` the `'complex` tag is stripped and passed to the complex package.
This package simply calls `magnitude` again which now strips either the `'rectangular` or `'polar` tag and passes it to the correct package.
A detailed trace is given below. I represent the complex numbers as a list instead of a pair here as this makes the trace cleaner.
```
(magnitude '(complex rectangular 3 4))
(apply-generic 'magnitude '(complex rectangular 3 4))
((get 'magnitude '(complex)) '(rectangular 3 4))
(magnitude '(rectangular 3 4))
(apply-generic 'magnitude '(rectangular 3 4))
((get 'magnitude '(rectangular)) '(3 4))
(magnitude-rectangular '(3 4))
-> 5
```
In particular note that `apply-generic` is called twice, first by the users call to the generic `magnitude` and secondly internal to the `complex` package.

Exercise 2.78
-------------

Implementation is given [here](scripts/Chapter2/exercise78.scm).

Exercise 2.79
-------------

Relevant changes are given [here](scripts/Chapter2/exercise79.scm).
To see the changes implemented see the [generic arithmetic program](scripts/bits/generic-arithmetic.scm).

Exercise 2.80
-------------

Relevant changes are given [here](scripts/Chapter2/exercise80.scm).
To see the changes implemented see the [generic arithmetic program](scripts/bits/generic-arithmetic.scm).

Exercise 2.81
-------------

#### a
Louis's coercion procedures create infinite loops in some cases.
Considering the example given of a generic `exp` procedure that is only defined for `'scheme-number`.
Calling this with two `'complex` arguments will fail to find a procedure and so attempt to coerce the arguments.
The types of the arguments are `'complex` and `'complex` so it will find the `complex->complex` coercion procedure.
This procedure, of course, does nothing and as such the following call to `apply-general` will have the same arguments.

#### b
Louis is not correct, the system only tries coercion if it fails to find a procedure that applies to the given types.
So long as coercions from a type to itself are not defined, everything works fine.

#### c
Implementation is given [here](scripts/Chapter2/exercise81.scm).

Exercise 2.82
-------------

Implementation is given [here](scripts/Chapter2/exercise82.scm).

As the only coercions this procedure tries are ones that make all the arguments the same type, it will not even try any mixed type operations that could potentionally work.
Say for example we are supplied a `scheme-number` and a `rational` as arguments and there is no matching operation.
If however, there is an operation that takes a `rational` and a `complex` as arguments, this procedure will never get tried despite `scheme-number` coercing to `rational`
and `rational` coercing to `complex`.

Exercise 2.83
-------------

Implementation is given [here](scripts/Chapter2/exercise83.scm).

Exercise 2.84
-------------

Implementation is given [here](scripts/Chapter2/exercise84.scm).

Exercise 2.85
-------------

Implementation is given [here](scripts/Chapter2/exercise85.scm).
The modifications to apply-generic simply require us to

Exercise 2.86
-------------

All we need do is replace the primitive operators in the definition of the `complex` package with generic versions.
This requires all the type coercions discused in exercises 2.83 to 2.85 to be implemented as well in order to allow complex numbers with mixed component types.

I would implement this right now, but I can't remember any trig identites that would make an implementation of trig functions for `rational` make sense.
