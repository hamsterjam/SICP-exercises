(define (double x)  (+ x x))
(define (halve x)   (/ x 2))

(define (fast-mul m n)
  (cond ((= n 0) 0)
        ((even? n) (double (fast-mul m (halve n))))
        (else (+ m (fast-mul m (- n 1))))))
