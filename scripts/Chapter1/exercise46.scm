(define (iterative-improve next good-enough?)
  (lambda (first-guess)
    (define (iter guess)
      (if (good-enough? guess)
          guess
          (iter (next guess))))
    (iter first-guess)))

(define (square x) (* x x))

(define (sqrt x)
  (define tolerance 0.00001)
  ((iterative-improve (lambda (y) (/ (+ y (/ x y)) 2))
                      (lambda (y) (< (abs (- (square y) x)) tolerance)))
   1.0))

(define (fixed-point f first-guess)
  (define tolerance 0.00001)
  ((iterative-improve f
                      (lambda (x) (< (abs (- (f x) x)) tolerance)))
   first-guess))
