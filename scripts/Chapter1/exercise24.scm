;
; Code for fast-prime?
;

(define (square x) (* x x))

(define (expmod base e m)
  (cond ((= e 0) 1)
        ((even? e)
         (remainder (square (expmod base (/ e 2) m))
                    m))
        (else
          (remainder (* base (expmod base (- e 1) m))
                     m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) #t)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else #f)))

;
; Code for timed-prime-test
;
(define (timed-prime-test n times)
  (display n)
  (start-prime-test n (runtime) times)
  (newline))

(define (start-prime-test n start-time times)
  (if (fast-prime? n times)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (run-test times)
  (timed-prime-test 1009 times)
  (timed-prime-test 1013 times)
  (timed-prime-test 1019 times)
  (newline)
  (timed-prime-test 10007 times)
  (timed-prime-test 10009 times)
  (timed-prime-test 10037 times)
  (newline)
  (timed-prime-test 100003 times)
  (timed-prime-test 100019 times)
  (timed-prime-test 100043 times)
  (newline)
  (timed-prime-test 1000003 times)
  (timed-prime-test 1000033 times)
  (timed-prime-test 1000037 times))
