(define (f-recur n)
  (if (< n 3)
      n
      (+ (f-recur (- n 1))
         (* 2 (f-recur (- n 2)))
         (* 3 (f-recur (- n 3))))))

(define (f-iter n)
  (define (iter a b c steps)
    (if (= steps n)
        a
        (iter (+ a (* 2 b) (* 3 c)) a b (+ steps 1))))
  (iter 2 1 0 2))
