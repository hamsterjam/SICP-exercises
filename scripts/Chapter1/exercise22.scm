;
; Code for prime?
;
(define (square x) (* x x))

(define (divides? m n)
  (= (remainder n m) 0))

(define (smallest-divisor n)
  (define (iter test)
    (cond ((> (square test) n) n)
          ((divides? test n) test)
          (else (iter (+ test 1)))))
  (iter 2))

(define (prime? n)
  (= (smallest-divisor n) n))

;
; Code for timed-prime-test
;
(define (timed-prime-test n)
  (display n)
  (start-prime-test n (runtime))
  (newline))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

;
; Code for search-for-primes
;
(define (search-for-primes first last)
  (cond ((even? first) (search-for-primes (+ first 1) last))
        ((> first last) #t)
        (else (timed-prime-test first)
              (search-for-primes (+ first 2) last))))

