(define (cbrt-iter guess prev-guess x)
  (if (good-enough? guess prev-guess)
      guess
      (cbrt-iter (improve guess x) guess x)))

(define (improve guess x)
  (/ (+ (/ x (* guess guess)) guess guess) 3))

(define (good-enough? guess prev-guess)
  (< (/ (abs (- prev-guess guess)) guess) 0.001))

(define (cbrt x) (cbrt-iter 1.0 2.0 x))
