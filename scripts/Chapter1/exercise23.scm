;
; Code for prime?
;
(define (square x) (* x x))

(define (divides? m n)
  (= (remainder n m) 0))

(define (smallest-divisor n)
  (define (iter test)
    (cond ((> (square test) n) n)
          ((divides? test n) test)
          (else (iter (next test)))))
  (iter 2))

(define (next n)
  (if (= n 2)
      3
      (+ n 2)))

(define (prime? n)
  (= (smallest-divisor n) n))

;
; Code for timed-prime-test
;
(define (timed-prime-test n)
  (display n)
  (start-prime-test n (runtime))
  (newline))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (run-test)
  (timed-prime-test 1009)
  (timed-prime-test 1013)
  (timed-prime-test 1019)
  (newline)
  (timed-prime-test 10007)
  (timed-prime-test 10009)
  (timed-prime-test 10037)
  (newline)
  (timed-prime-test 100003)
  (timed-prime-test 100019)
  (timed-prime-test 100043)
  (newline)
  (timed-prime-test 1000003)
  (timed-prime-test 1000033)
  (timed-prime-test 1000037))
