(define (id x) x)
(define (inc x) (+ x 1))
(define (square x) (* x x))

(define (prod-iter term a next b)
  (define (iter a res)
    (if (> a b)
        res
        (iter (next a) (* res (term a)))))
  (iter a 1))

(define (prod-recur term a next b)
  (if (> a b)
      1
      (* (term a)
         (prod-recur term (next a) next b))))

(define prod prod-iter)


(define (fact n)
  (prod id 1 inc n))

; I modified the product a little
(define (pi-prod n)
  (define (term n) (square (/ n (- n 1))))
  (define (next n) (+ n 2))
  (* (/ 8.0 n) (prod term 4.0 next n)))

