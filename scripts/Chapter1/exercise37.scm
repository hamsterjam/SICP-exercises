(define (cont-frac-iter n d k)
  (define (iter res i)
    (if (= i 0)
        res
        (iter (/ (n i)
                 (+ (d i) res))
              (- i 1))))
  (iter 0 k))

(define (cont-frac-recur n d k)
  (define (recur i)
    (if (> i k)
        0
        (/ (n i)
           (+ (d i)
              (recur (+ i 1))))))
  (recur 1))

(define cont-frac cont-frac-iter)

(define (test k)
  (display k)
  (display ":\t")
  (display (cont-frac (lambda (i) 1.0)
                      (lambda (i) 1.0)
                      k))
  (newline))

(define (do-tests-till k)
  (define (iter i)
    (cond ((<= i k)
           (test i)
           (iter (+ i 1)))))
  (iter 1))
