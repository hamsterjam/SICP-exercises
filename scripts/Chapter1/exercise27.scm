(define (square x) (* x x))

(define (expmod base e m)
  (cond ((= e 0) 1)
        ((even? e)
         (remainder (square (expmod base (/ e 2) m))
                    m))
        (else
          (remainder (* base (expmod base (- e 1) m))
                     m))))

(define (complete-fermat-test n)
  (define (iter a)
    (cond ((= a n) #t)
          ((= (expmod a n n) a) (iter (+ a 1)))
          (else #f)))
  (iter 1))

(define (print-test-case n)
  (display n)
  (display "\t")
  (if (complete-fermat-test n)
      (display "pass")
      (display "fail"))
  (newline))

(define (run-tests)
  (print-test-case 561)
  (print-test-case 1105)
  (print-test-case 1729)
  (print-test-case 2465)
  (print-test-case 2821)
  (print-test-case 6601))

