; Returns the number on the mth row in the nth position (0 indexed) of Pascal's triangle
(define (pascal m n)
  (cond ((= n 0) 1)
        ((= n m) 1)
        (else (+ (pascal (- m 1) n)
                 (pascal (- m 1) (- n 1))))))
