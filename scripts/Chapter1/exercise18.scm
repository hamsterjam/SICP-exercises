(define (double x) (+ x x))
(define (halve x)  (/ x 2))

(define (fast-mul-iter m n)
  (define (iter a m n)
    (cond ((= n 0) a)
          ((even? n) (iter a (double m) (halve n)))
          (else (iter (+ a m) m (- n 1)))))
  (iter 0 m n))
