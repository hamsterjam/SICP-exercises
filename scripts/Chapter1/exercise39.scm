(define (tan-cf x k)
  (define (recur i x-to-i)
    (if (> i k)
        0
        (/ x-to-i
           (- (* 2 i) 1
              (recur (+ i 1)
                     (* x-to-i x))))))
  (recur 1.0 x))
