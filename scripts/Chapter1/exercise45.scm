(define (comp f g)
  (lambda (x)
    (f (g x))))

(define (repeated f n)
  (cond ((< n 1) (lambda (x) x))
        ((= n 1) f)
        (else (comp f (repeated f (- n 1))))))

(define tolerance 0.00001)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (average-damp f)
  (lambda (x)
    (/ (+ x (f x)) 2)))

(define (root-explicit-damps n x damps)
  (fixed-point ((repeated average-damp damps)
                (lambda (y) (/ x (expt y (- n 1)))))
               1.0))

(define (root n x)
  (root-explicit-damps n x (floor (/ (log n) (log 2)))))
