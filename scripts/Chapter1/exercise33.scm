(define (filtered-accumulate filt comb null term a next b)
  (define (iter a res)
    (cond ((> a b) res)
          ((filt a) (iter (next a) (comb res (term a))))
          (else (iter (next a) res))))
  (iter a null))

(define (square x) (* x x))
(define (inc x) (+ x 1))
(define (id x) x)

; Sum of the squares of all primes in an interval
(define (divides? m n)
  (= (remainder n m) 0))

(define (smallest-divisor n)
  (define (iter test)
    (cond ((> (square test) n) n)
          ((divides? test n) test)
          (else (iter (+ test 1)))))
  (iter 2))

(define (prime? n)
  (= (smallest-divisor n) n))

(define (sum-prime-squares a b)
  (filtered-accumulate
    prime?
    +
    0
    square
    a
    inc
    b))

; Product of all positive integers smaller than and co-prime to n
(define (coprime-prod n)
  (filtered-accumulate
    (lambda (x) (= (gcd n x) 1))
    *
    1
    id
    2
    inc
    n))
