(define (equ? x y) (apply-generic 'equ? x y))

; In the scheme-number package
(put 'equ? '(scheme-number scheme-number) =)

; In the rational package
(define (equ? x y)
  (= (* (numer x) (denom y))
     (* (denom x) (numer y))))
(put 'equ? '(rational rational) equ?)

; In the complex package
(define (equ? x y)
  (and (= (real-part x) (real-part y))
       (= (imag-part x) (imag-part y))))
(put 'equ? '(complex complex) equ?)
