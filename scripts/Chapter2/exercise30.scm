(define (square x) (* x x))

(define (square-tree1 T)
  (cond ((null? T) '())
        ((not (pair? T)) (square T))
        ((cons (square-tree1 (car T))
               (square-tree1 (cdr T))))))

(define (square-tree2 T)
  (map (lambda (S)
         (if (pair? S)
             (square-tree2 S)
             (square S)))
       T))
