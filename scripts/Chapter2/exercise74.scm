(define get '())
(define put '())
(load "../bits/getput.scm")

; Tag procedures

(define (attach-tag type-tag contents)
  (cons type-tag contents))

(define (type-tag datum)
  (if (pair? datum)
      (car datum)
      (error "Bad tagged datum - TYPE-TAG" datum)))

(define (contents datum)
  (if (pair? datum)
      (cdr datum)
      (error "Bad tagged datum - CONTENTS" datum)))

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (error
            "No method for these types - APPLY-GENERIC"
            (list op type-tags))))))

; Query procedures

(define (get-record employee-name personnel-file)
  (apply-generic 'get-record
                 (attach-tag 'name employee-name)
                 personnel-file))

(define (get-salary employee-record)
  (apply-generic 'get-salary
                 employee-record))

(define (find-employee employee-name personnel-files)
  (if (null? personnel-files)
      #f
      (let ((employee (get-record employee-name (car personnel-files))))
        (if employee
            employee
            (find-employee employee-name (cdr personnel-files))))))
