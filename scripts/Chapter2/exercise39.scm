(define (fold-right op init seq)
  (if (null? seq)
      init
      (op (car seq)
          (fold-right op init (cdr seq)))))

(define (fold-left op init seq)
  (define (iter res seq)
    (if (null? seq)
        res
        (iter (op res (car seq))
              (cdr seq))))
  (iter init seq))

(define (reverse1 seq)
  (fold-right (lambda (x y) (append y (list x))) '() seq))

(define (reverse2 seq)
  (fold-left (lambda (x y) (cons y x)) '() seq))
