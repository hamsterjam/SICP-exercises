; Importantly, this does not use apply-generic
(define (raise x) ((get 'raise (list (type-tag x))) x))

; In integer package
(define (raise x)
  ((get 'make 'rational) x 1))
(put 'raise '(integer) raise)

; In rational package
(define (raise x)
  ((get 'make 'real) (/ (number x) (denom x))))
(put 'raise '(rational) raise)

; In real package
(define (raise x)
  ((get 'make 'complex) x 0))
(put 'raise '(real) raise)
