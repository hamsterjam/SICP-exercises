(define (sign x)
  (if (< x 0)
      -1
      1))

(define (make-rat n d)
  (let ((g (gcd n d))
        (s (sign d)))
    (cons (/ (* n s) g)
          (/ (* d s) g))))
