;
; First we need a generic project operation
;

(define (project x) (apply-generic 'project x))

; In rational package
(define (project x)
  ((get 'make 'integer) (round (/ (numer x) (denom x)))))
(put 'project '(rational) project)

; In real package
(define (project x)
  ((get 'make 'rational) (round x) 1))
(put 'project '(real) project)

; In complex package
(define (project x)
  ((get 'make 'real) (real-part x)))
(put 'project '(complex) project)

;
; Now drop is simple to define
;

(define (drop x)
  (let ((projected (project x)))
    (let ((test (raise projected)))
      (if (equ? x test)
          (drop projected)
          x))))
