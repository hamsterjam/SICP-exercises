
; List functions

(define (list-find L x)
  (define (iter L i)
    (cond ((= (car L) x) i)
          ((null? (cdr L)) -1)
          (else (iter (cdr L) (+ i 1)))))
  (iter L 0))

(define (list-contains? L x)
  (>= (list-find L x) 0))

(define (list-intersection K L)
  (cond ((null? K) '())
        ((list-contains? L (car K)) (cons (car K) (list-intersection (cdr K) L)))
        (else (list-intersection (cdr K) L))))

(define (list-remove-index L i)
  (if (= i 0)
      (cdr L)
      (cons (car L) (list-remove-index (cdr L) (- i 1)))))

(define (list-remove-value L x)
  (list-remove-index L (list-find L x)))

(define (list-cat K L)
  (if (null? K)
      L
      (cons (car K) (list-cat (cdr K) L))))

; BinTree functions

(define (bin-tree-min T)
  (if (not (pair? T))
      T
      (min (bin-tree-min (car T))
           (bin-tree-min (cdr T)))))

(define (bin-tree-max T)
  (if (not (pair? T))
      T
      (max (bin-tree-max (car T))
           (bin-tree-max (cdr T)))))

(define (bin-tree-map T f)
  (if (not (pair? T))
      (f T)
      (cons (bin-tree-map (car T) f)
            (bin-tree-map (cdr T) f))))

(define (bin-tree-split T i)
  (define (iter T i f)
    (if (= i 0)
        (f T)
        (cons (iter (car T) (- i 1) f)
              (iter (cdr T) (- i 1) f))))
  (if (not (pair? T))
      (cons T T)
      (cons (iter T i car)
            (iter T i cdr))))

(define (bin-tree-cat S T f)
  (bin-tree-map S (lambda (x)
                    (bin-tree-map T (lambda (y)
                                      (f x y))))))

; Interval functions

(define make-interval
  ((lambda ()
     (define i 0)
     (lambda (a b)
       (set! i (+ i 1))
       (cons (list i) (cons a b))))))

(define (lower-bound I) (bin-tree-min (cdr I)))
(define (upper-bound I) (bin-tree-max (cdr I)))

(define (make-center-width c w)
  (make-interval (- c w)
                 (+ c w)))

(define (center I)
  (/ (+ (lower-bound I)
        (upper-bound I))
     2))

(define (width I)
  (/ (- (upper-bound I)
        (lower-bound I))
     2))

(define (make-center-percent c p)
  (make-center-width c (* (/ p 100.0) c)))

(define (percent I)
  (* (/ (width I) (center I)) 100.0))

(define (op-interval f I J)
  (let ((I-names  (car I))
        (J-names  (car J))
        (I-values (cdr I))
        (J-values (cdr J)))
    (let ((name-clash (list-intersection I-names J-names)))
      (if (null? name-clash)
          (cons (list-cat I-names J-names)
                (bin-tree-cat I-values J-values f))
          (let ((I-names (list-remove-value I-names (car name-clash)))
                (J-names (list-remove-value I-names (car name-clash)))
                (I-split (bin-tree-split I-values (list-find I-names (car name-clash))))
                (J-split (bin-tree-split J-values (list-find J-names (car name-clash)))))
            (let ((I-left  (cons I-names (car I-split)))
                  (I-right (cons I-names (cdr I-split)))
                  (J-left  (cons J-names (car J-split)))
                  (J-right (cons J-names (cdr J-split))))
              (let ((res-left  (op-interval f I-left  J-left))
                    (res-right (op-interval f I-right J-right)))
                (cons (cons (car name-clash) (car res-left))
                      (cons (cdr res-left) (cdr res-right))))))))))

(define (print-interval I)
  (display "(")
  (display (lower-bound I))
  (display ", ")
  (display (upper-bound I))
  (display ")")
  (newline))

(define (interval-includes? I x)
  (and (<= (lower-bound I) x)
       (>= (upper-bound I) x)))

(define (add-interval I J) (op-interval + I J))
(define (sub-interval I J) (op-interval - I J))
(define (mul-interval I J) (op-interval * I J))

(define (div-interval I J)
  (if (interval-includes? J 0)
      (error "Division by interval that includes 0")
      (op-interval / I J)))

(define (par1 r1 r2)
  (div-interval (mul-interval r1 r2)
                (add-interval r1 r2)))

(define (par2 r1 r2)
  (let ((one (make-interval 1 1)))
    (div-interval one
                  (add-interval (div-interval one r1)
                                (div-interval one r2)))))
