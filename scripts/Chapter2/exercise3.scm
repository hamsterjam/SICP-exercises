(define (make-point x y) (cons x y))
(define (x-point p) (car p))
(define (y-point p) (cdr p))

; Represented as 2 points in space, normalised so the y and x components of p
; are both less than those of q
(define (make-rect p q)
  (let ((px (x-point p))
        (py (y-point p))
        (qx (x-point q))
        (qy (y-point q)))
    (cons (make-point (min px qx)
                      (min py qy))
          (make-point (max px qx)
                      (max py qy)))))

(define (width-rect R)
  (- (x-point (cdr R))
     (x-point (car R))))

(define (height-rect R)
  (- (y-point (cdr R))
     (y-point (car R))))

; Alternative representation where the order of the components of p and q are
; left however they are supplied, width and height are normalised on request
(define (make-rect p q)
  (cons p q))

(define (width-rect R)
  (abs (- (x-point (car R))
          (x-point (cdr R)))))

(define (height-rect R)
  (abs (- (y-point (car R))
          (y-point (cdr R)))))

; The following procedures work the same regardless of which representation you
; choose to use
(define (perimeter-rect R)
  (* 2 (+ (width-rect R) (height-rect R))))

(define (area-rect R)
  (* (width-rect R) (height-rect R)))
