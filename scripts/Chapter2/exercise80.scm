(define (=zero? x) (apply-generic '=zero? x))

; In the scheme-number package
(put '=zero? '(scheme-number)
     (lambda (x) (= x 0)))

; In the rational package
(define zero (make-rat 0 1))
(put '=zero? '(rational)
     (lambda (x) (equ? x zero)))

; In the complex package
(define zero (make-from-real-imag 0 0))
(put '=zero? '(complex)
     (lambda (x) (equ? x zero)))
