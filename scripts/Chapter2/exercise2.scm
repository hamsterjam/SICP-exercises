(define (make-segment p q) (cons p q))
(define (start-segment s) (car s))
(define (end-segment s)   (cdr s))

(define (make-point x y) (cons x y))
(define (x-point p) (car p))
(define (y-point p) (cdr p))

(define (add-point p q)
  (make-point (+ (x-point p) (x-point q))
              (+ (y-point p) (y-point q))))

(define (div-point p x)
  (make-point (/ (x-point p) x)
              (/ (y-point p) x)))

(define (print-point p)
  (display "(")
  (display (x-point p))
  (display ", ")
  (display (y-point p))
  (display ")")
  (newline))

(define (midpoint-segment s)
  (div-point (add-point (start-segment s)
                        (end-segment s))
             2))
