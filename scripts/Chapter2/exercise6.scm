(define church-0 (lambda (f) (lambda (x) x)))
(define church-1 (lambda (f) (lambda (x) (f x))))
(define church-2 (lambda (f) (lambda (x) (f (f x)))))
(define church-3 (lambda (f) (lambda (x) (f (f (f x))))))

(define (church-inc n)
  (lambda (f) (lambda (x) (f ((n f) x)))))

(define (church-add m n)
  (lambda (f) (lambda (x) ((m f) ((n f) x)))))

(define (church-mul m n)
  (lambda (f) (lambda (x) ((m (n f)) x))))

(define (church-expt m n)
  (lambda (f) (lambda (x) (((n m) f) x))))

(define (church n)
  (if (= n 0)
      church-0
      (church-inc (church (- n 1)))))

(define (unchurch n)
  ((n (lambda (x) (+ x 1))) 0))

