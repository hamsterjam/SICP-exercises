(define (lookup given-key set-of-records)
  (if (null? set-of-records)
      #f
      (let ((this-key (key (entry set-of-records))))
        (cond ((= given-key this-key)
               (entry set-of-records))
              ((< given-key this-key)
               (lookup given-key
                       (left-branch set-of-records)))
              ((> given-key this-key)
               (lookup given-key
                       (right-branch set-of-records)))))))
