(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq)
          (accumulate op init (cdr seq)))))

(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

(define (filter pred seq)
  (cond ((null? seq) '())
        ((pred (car seq)) (cons (car seq) (filter pred (cdr seq))))
        (else (filter pred (cdr seq)))))

(define (enumerate-interval i f)
  (if (> i f)
      (list f)
      (cons i (enumerate-interval (+ i 1) f))))

(define (unique-pairs n)
  (flatmap
    (lambda (x)
      (map (lambda (y) (list x y))
           (enumerate-interval 1 x)))
    (enumerate-interval 1 n)))

(define (square x) (* x x))

(define (prime? p)
  (define (iter i)
    (cond ((> (square i) p) #t)
          ((= 0 (remainder p i)) #f)
          (else (iter (+ i 1)))))
  (iter 2))

(define (prime-sum? p)
  (prime? (+ (car p) (cadr p))))

(define (make-pair-sum p)
  (list (car p) (cadr p) (+ (car p) (cadr p))))

(define (prime-sum-pairs n)
  (map make-pair-sum
       (filter prime-sum?
               (unique-pairs n))))
