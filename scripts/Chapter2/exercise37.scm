(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq)
          (accumulate op init (cdr seq)))))

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))

(define (dot-product p q)
  (accumulate + 0 (map * p q)))

(define (matrix-*-vector A p)
  (map (lambda (x) (dot-product x p)) A))

(define (transpose A)
  (accumulate-n cons `() A))

(define (matrix-*-matrix A B)
  (let ((cols (transpose B)))
    (map (lambda (x) (matrix-*-vector cols x)) A)))
