(define (generate-huffman-tree pairs)
  (succesive-merge (make-leaf-set pairs)))

(define (succesive-merge set)
  (if (= (length set) 1)
      (car set)
      (succesive-merge
        (adjoin-set (make-code-tree (car set)
                                    (cadr set))
                    (cddr set)))))
