(define (tree-map f T)
  (map (lambda (S)
         (if (pair? S)
             (tree-map f S)
             (f S)))
       T))

(define (square x) (* x x))
(define (square-tree T) (tree-map square T))
