(define (next-type x) (get 'next-type x))

; In integer package
(put 'next-type 'integer 'rational)
; In rational package
(put 'next-type 'rational 'real)
; In real package
(put 'next-type 'real 'complex)

(define (can-raise-to? from-type to-type)
  (if (eq? from-type to-type)
      #t
      (let ((next (next-type from-type)))
        (if next
            (can-raise-to? next from-type)
            #f))))

(define (list-and vals)
  (cond ((null? vals) #t)
        ((car vals) (list-and (cdr vals)))
        (else #f)))

(define (highest-type types)
  (define (iter yet-to-test)
    (if (null? yet-to-test)
        (error "No highest type - HIGHEST-TYPE" types)
        (if (list-and (map (lambda (x) (can-raise-to? x (car yet-to-test))) types))
            (car types)
            (iter (cdr yet-to-test)))))
  (iter types))

; assumes that it is possible
(define (raise-to val type)
  (if (eq? (type-tag val) type)
      val
      (raise val)))

(define (apply-generic op . args)
  (let ((type-tags (map (type-tag args))))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (let ((highest-tag (highest-type type-tags)))
            (apply apply-generic
                   (cons op
                         (map (lambda (x) (raise-to x highest-tag))
                              args))))))))

