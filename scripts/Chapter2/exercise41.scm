(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq)
          (accumulate op init (cdr seq)))))

(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

(define (filter pred seq)
  (cond ((null? seq) '())
        ((pred (car seq)) (cons (car seq) (filter pred (cdr seq))))
        (else (filter pred (cdr seq)))))

(define (enumerate-interval i f)
  (if (> f i)
      (cons i (enumerate-interval (+ i 1) f))
      (list f)))

(define (unique-pairs n)
  (flatmap
    (lambda (x)
      (map (lambda (y) (list x y))
           (enumerate-interval 1 x)))
    (enumerate-interval 1 n)))

(define (unique-triples n)
  (flatmap
    (lambda (x)
      (map (lambda (y) (cons x y))
           (unique-pairs x)))
    (enumerate-interval 1 n)))

(define (triple-sums? t s)
  (= s (+ (car t) (cadr t) (caddr t))))

(define (triples-summing s n)
  (filter
    (lambda (t) (triple-sums? t s))
    (unique-triples n)))

