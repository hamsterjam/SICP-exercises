(define (for-each f L)
  (cond ((null? L) #t) ;This is an arbitrary return value
        (else
          (f (car L))
          (for-each f (cdr L)))))
