(define get '())
(define put '())
(load "../bits/getput.scm")

(define (=number? x y)
  (and (number? x) (number? y) (= x y)))

(define (variable? x) (symbol? x))
(define (=variable? v1 v2)
  (and (variable? v1) (variable? v2) (eq? v1 v2)))

(define (deriv expr var)
  (cond ((number? expr) 0)
        ((variable? expr)
         (if (=variable? expr var) 1 0))
        (else
          ((get 'deriv (operator expr))
           (operands expr)
           var))))

(define (operator expr) (car expr))
(define (operands expr) (cdr expr))

(define (install-sum-package)
  ;; internal procedures
  (define (make-sum a1 a2)
    (cond ((=number? a1 0) a2)
          ((=number? a2 0) a1)
          ((and (number? a1) (number? a2)) (+ a1 a2))
          (else (list '+ a1 a2))))
  (define (augend s) (car s))
  (define (addend s) (cadr s))
  (define (deriv-sum s var)
    (make-sum (deriv (augend s) var)
              (deriv (addend s) var)))

  ;; interface
  (put 'make '+ make-sum)
  (put 'deriv '+ deriv-sum)
  'done)

(define (install-product-package)
  ;; internal procedures
  (define (make-product m1 m2)
    (cond ((or (=number? m1 0) (=number? m2 0)) 0)
          ((=number? m1 1) m2)
          ((=number? m2 1) m1)
          ((and (number? m1) (number? m2)) (* m1 m2))
          (else (list '* m1 m2))))
  (define (multiplier   p) (car p))
  (define (multiplicand p) (cadr p))
  (define (deriv-product p var)
    ((get 'make '+)
     (make-product (multiplier p)
                   (deriv (multiplicand p) var))
     (make-product (deriv (multiplier p) var)
                   (multiplicand p))))

  ;; interface
  (put 'make '* make-product)
  (put 'deriv '* deriv-product)
  'done)

(define (install-exponentiation-package)
  ;; interntal procedures
  (define (make-exponentiation b e)
    (cond ((=number? e 0) 1)
          ((=number? e 1) b)
          ((and (number? e) (number? b) (expt b e)))
          (else (list '** b e))))
  (define (base e)     (car e))
  (define (exponent e) (cadr e))
  (define (deriv-exponentiation e var)
    (let ((make-prod (get 'make '*))
          (make-sum  (get 'make '+)))
      (make-prod
        (make-prod (exponent e)
                   (make-exponentiation (base e)
                                        (make-sum (exponent e) -1)))
        (deriv (base e) var))))

  ;; interface
  (put 'make '** make-exponentiation)
  (put 'deriv '** deriv-exponentiation)
  'done)

(install-sum-package)
(install-product-package)
(install-exponentiation-package)
