(define (get-coercion-or-identity t1 t2)
  (if (eq? t1 t2)
      (lambda (x) x)
      (get-coercion t1 t2)))

(define (apply-each procs vals)
  (if (or (null? procs) (null? vals))
      '()
      (cons ((car procs) (car vals))
            (apply-each (cdr procs) (cdr vals)))))

(define (contains? key vals)
  (cond ((null? vals) #f)
        ((= (car vals) key) #t)
        (else (contains? key (cdr vals)))))

(define (all-same? vals)
  (cond ((null? vals) #t)
        ((null? (cdr vals) #t))
        (else (not (contains? (car vals) (cdr vals))))))

(define (coerce-args type args)
  (define (get-coercion-to-type arg)
    (get-coercion-or-identity (type-tag arg) type))
  (let ((coercions (map get-coercion-to-type args)))
    (if (contains? coercions #f)
        #f
        (apply-each coercions args))))

(define (apply-generic op . args)
  (define (try-coercions types)
    (if (null? types)
        (error "No method for these types - APPLY-GENERIC"
               (list op (map type-tag args)))
        (let ((coerced (coerce-args (car types) args)))
          (if coerced
              coerced
              (try-coercions op (cdr types))))))

  (let ((type-tags (map (type-tag args))))
    (let ((proc (get op type-tags)))
      (cond (proc
              (apply proc (map contents args)))
            ((all-same? type-tags)
              (error "No method for these types - APPLY-GENERIC"
                     (list op (type-tags))))
            (else (apply apply-generic (cons op (try-coercions type-tags))))))))
