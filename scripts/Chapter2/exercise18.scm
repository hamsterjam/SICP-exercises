(define (reverse L)
  (define (iter L res)
    (if (null? L)
        res
        (iter (cdr L)
              (cons (car L) res))))
  (iter L '()))
