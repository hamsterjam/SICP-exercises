(define (filter pred seq)
  (cond ((null? seq) '())
        ((pred (car seq)) (cons (car seq) (filter pred (cdr seq))))
        (else (filter pred (cdr seq)))))

(define (fold-right op init seq)
  (if (null? seq)
      init
      (op (car seq)
          (fold-right op init (cdr seq)))))

(define (flatmap f seq)
  (fold-right append '() (map f seq)))

(define (enumerate-interval i j)
  (if (< i j)
      (cons i (enumerate-interval (+ i 1) j))
      (list j)))

(define (queens size-of-board)
  (define (safe? board)
    (let ((pos (car board)))
      (define (iter i board)
        (cond ((null? board) #t)
              ((or (= (car board) pos)
                   (= (abs (- (car board) pos)) i))
                #f)
              (else (iter (+ i 1) (cdr board)))))
      (iter 1 (cdr board))))

  (define (queen-cols k)
    (if (= k 0)
        (list '())
        (filter
          safe?
          (flatmap (lambda (board)
                     (map (lambda (pos)
                            (cons pos board))
                          (enumerate-interval 1 size-of-board)))
                   (queen-cols (- k 1))))))
  (queen-cols size-of-board))

(define (print-board board)
  (define (divider n)
    (for-each
      (lambda (i)
        (display "+---"))
      (enumerate-interval 1 n))
    (display "+"))

  (define (print-blanks n)
    (cond ((= n 0) (display "|"))
          (else
            (for-each
              (lambda (i)
                (display "|   "))
              (enumerate-interval 1 n))
            (display "|"))))

  (let ((board-size (length board)))
    (for-each
      (lambda (pos)
        (divider board-size)
        (newline)
        (print-blanks (- pos 1))
        (display " # ")
        (print-blanks (- board-size pos))
        (newline))
      board)
    (divider board-size)
    (newline)
    (newline)))
