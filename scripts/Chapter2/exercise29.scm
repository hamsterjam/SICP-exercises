(define (make-mobile left right)
  (list left right))

(define (make-branch len struct)
  (list len struct))

(define (left-branch M)
  (car M))

(define (right-branch M)
  (cadr M))

(define (branch-length B)
  (car B))

(define (branch-struct B)
  (cadr B))

(define (total-weight M)
  (if (not (pair? M))
      M
      (+ (total-weight (branch-struct (left-branch  M)))
         (total-weight (branch-struct (right-branch M))))))

(define (is-balanced? M)
  (if (not (pair? M))
      #t
      (let ((Bl (left-branch  M))
            (Br (right-branch M)))
        (and (is-balanced? (branch-struct Bl))
             (is-balanced? (branch-struct Br))
             (= (* (branch-length Bl)
                   (total-weight (branch-struct Bl)))
                (* (branch-length Br)
                   (total-weight (branch-struct Br))))))))
