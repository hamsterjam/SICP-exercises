(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq)
          (accumulate op init (cdr seq)))))

(define (horner-eval x coefs)
  (accumulate (lambda (this-coef higher-terms)
                (+ (* higher-terms x) this-coef))
              0
              coefs))
