(define (in? x set)
  (cond ((null? set) #f)
        ((eq? x (car set)) #t)
        (else (in? x (cdr set)))))

(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (encode-symbol symbol tree)
  (if (leaf? tree)
      '()
      (let ((left  (left-branch  tree))
            (right (right-branch tree)))
        (cond ((in? symbol (symbols left))
               (cons 0
                     (encode-symbol symbol left)))
              ((in? symbol (symbols right))
               (cons 1
                     (encode-symbol symbol right)))
              (else (error "bad symbol - ENCODE-SYMBOL" symbol))))))
