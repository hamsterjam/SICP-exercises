(define (last-pair L)
  (cond ((null? L) L)
        ((null? (cdr L)) L)
        (else (last-pair (cdr L)))))
