(define (same-parity a . L)
  (define (iter par L)
    (cond ((null? L)
            '())
          ((= (remainder (car L) 2) par)
            (cons (car L) (iter par (cdr L))))
          (else
            (iter par (cdr L)))))
  (cons a (iter (remainder a 2) L)))
