(define (=number? expr num) (and (number? expr) (= expr num)))

(define (in? item seq)
  (cond ((null? seq) #f)
        ((eq? (car seq) item) #t)
        (else (in? item (cdr seq)))))

(define (split seq item)
  (cond ((null? seq) '(()()))
        ((eq? (car seq) item) (list '() (cdr seq)))
        (else (let ((rest (split (cdr seq) item)))
                (let ((before (car rest))
                      (after  (cadr rest)))
                  (list (cons (car seq) before) after))))))

(define (unwrap-if-single x)
  (if (= (length x) 1)
      (car x)
      x))

(define (deriv expr var)
  (cond ((number? expr) 0)
        ((variable? expr)
         (if (same-variable? expr var) 1 0))
        ((sum? expr)
         (make-sum (deriv (addend expr) var)
                   (deriv (augend expr) var)))
        ((product? expr)
         (make-sum
           (make-product (multiplier expr)
                         (deriv (multiplicand expr) var))
           (make-product (deriv (multiplier expr) var)
                         (multiplicand expr))))
        ((exponentiation? expr)
         (make-product
           (make-product (exponent expr)
                         (make-exponentiation (base expr)
                                              (make-sum (exponent expr) -1)))
           (deriv (base expr) var)))
        (else
          (error "unknown expression type - DERIV" expr))))

(define (variable? x) (symbol? x))

(define (same-variable? v1 v2)
  (and (variable? v1) (variable? v2) (eq? v1 v2)))

; Sums

(define (sum? x)
  (and (pair? x) (in? '+ x)))

(define (make-sum a1 a2)
  (let ((expand1 (or (sum? a1) (product? a1)))
        (expand2 (or (sum? a2) (product? a2))))
    (cond ((=number? a1 0) a2)
          ((=number? a2 0) a1)
          ((and (number? a1) (number? a2)) (+ a1 a2))

          ((and expand1 expand2) (append a1 (cons '+ a2)))
          (expand1 (append a1 (list '+ a2)))
          (expand2 (append (list a1 '+) a2))

          (else (list a1 '+ a2)))))

(define (augend s)
  (unwrap-if-single (car (split s '+))))

(define (addend s)
  (unwrap-if-single (cadr (split s '+))))

; Products

(define (product? x)
  (and (pair? x) (not (sum? x)) (in? '* x)))

(define (make-product m1 m2)
  (let ((expand1 (product? m1))
        (expand2 (product? m2)))
    (cond ((or (=number? m1 0) (=number? m2 0)) 0)
          ((=number? m1 1) m2)
          ((=number? m2 1) m1)
          ((and (number? m1) (number? m2)) (* m1 m2))

          ((and expand1 expand2) (append m1 (cons '* m2)))
          (expand1 (append m1 (list '* m2)))
          (expand2 (append (list m1 '*) m2))

          (else (list m1 '* m2)))))

(define (multiplier p)
  (unwrap-if-single (car (split p '*))))

(define (multiplicand p)
  (unwrap-if-single (cadr (split p '*))))

; Exponentiations

(define (exponentiation? x)
  (and (pair? x) (eq? (cadr x) '**)))

(define (make-exponentiation b e)
  (cond ((=number? e 0) 1)
        ((=number? e 1) b)
        ((and (number? b) (number? e) (expt b e)))
        (else (list b '** e))))

(define (base e) (car e))
(define (exponent e) (caddr e))
