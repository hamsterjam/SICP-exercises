Exercise 1.1
------------

Console output is prefixed with `->`.

```
10
-> 10

(+ 5 3 4)
-> 12

(- 9 1)
-> 8

(/ 6 2)
-> 3

(+ (* 2 4) (- 4 6))
-> 6

(define a 3)
(define b (+ a 1))
(+ a b (* a b))
-> 19

(= a b)
-> false

(if (and (> b a) (< b (* a b)))
    b
    a)
-> 4

(cond ((= a 4) 6)
      ((= b 4) (+ 6 7 a))
      (else 25))
-> 16

(+ 2 (if (> b a) b a))
-> 6

(* (cond ((> a b) a)
         ((< a b) b)
         (else -1))
   (+ a 1))
-> 16
```

Exercise 1.2
------------

```
(/ (+ 5 4
      (- 2
         (- 3
            (+ 6 (/ 4 5)))))
   (* 3
      (- 6 2)
      (- 2 7)))
```

Exercise 1.3
------------

Implementation is given [here](scripts/Chapter1/exercise3.scm).

Exercise 1.4
------------

The given procedure adds the first argument to the absolute value of the second argument.

Exercise 1.5
------------

First note that `p` is defined as a function that calls itself.
If the interpreter ever attempts to call `p` it will enter an infinite loop.
If applicative-order evaluation, the operands are evaluated first, notably attempting to call `p`.
Thus if the interpreter is using applicative-order, it will enter an infinite loop and fail to return any value.

If however normal-order evaluation is used, we first expand the operators giving
```
(if (= 0 0) 0 (p))
```
The `if` special form is not evaluated in the regular manner; if it were, the next step would be to evaluate `(p)` triggering the infinite loop.
Instead the predicate is evaluated to `true` so the first expression, 0, is returned without ever evaluating `(p)`.
Thus the result for an interpreter using normal-order is 0.

Exercise 1.6
------------

If the interpreter were to use normal-order, all would be fine as `new-if` would "inherit" the special evaluation order from the `cond` expression used to define it.
However, if the interpreter were to use applicative-order, all the operands of `new-if` would first be evaluated, regardless of the value of the predicate.
Notably, `sqrt-iter` contains a call to `new-if` which has a call to `sqrt-iter` as one of its operands; this gives rise to an infinite loop.
If the regular `if` special form were to be used instead, the predicate would be evaluated first, and only if it failed would the next call to `sqrt-iter` be evaluated.


Exercise 1.7
------------

If a number is significantly smaller than the threshold, then numbers of magnitude near the square of the threshold will pass the given implementation of `good-enough?`.
For example, If we attempt to take the square root of $`1 \times 10^{-100}`$ we get an answer of $`0.03125`$.
Significantly different, some would argue, from the expected result of $`1 \times 10^{-50}`$.

If however we choose numbers so large that the gap between adjacent numbers is larger than the threshold value, we encounter another problem.
Numbers this large would require a perfect square root, one that when squared results in the original number exactly; this may not always even be possible.
For example, using the given procedure for calculating square roots on $`2 \times 10^{300}`$ fails to produce a result.

For an implementation of the improved procedure discussed in the problem, see [here](scripts/Chapter1/exercise7.scm).

The new procedure neatly solves both problems by comparing the error to a value that has the same magnitude as itself (namely, itself).
It produces sensible results for both examples given above.
The reported square root of $`1 \times 10^{-100}`$ is $`1.0000000000002005 \times 10^{-50}`$,
and the reported square root of $`2 \times 10^{300}`$ is $`1.4142135880577324 \times 10^{150}`$.

Exercise 1.8
------------

Implementation is given [here](scripts/Chapter1/exercise8.scm).

Exercise 1.9
------------

Using the substitution model on the first procedure we get
```
(+ 4 5)
(inc (+ 3 5))
(inc (inc (+ 2 5)))
(inc (inc (inc (+ 1 5))))
(inc (inc (inc (inc (+ 0 5)))))
(inc (inc (inc (inc 5))))
(inc (inc (inc 6)))
(inc (inc 7))
(inc 8)
9
```
Notice that we get a chain of deferred `inc` operations and the "growing" and "shrinking" described in the factorial example.
Thus this proces is recursive.

Using the substitution model on the second procedure we get
```
(+ 4 5)
(+ 3 6)
(+ 2 7)
(+ 1 8)
(+ 0 9)
9
```
We see that there is no deferred operations, instead we see that the processes is summarized by the state variables `a` and `b`.
Thus this process is iterative.

Exercise 1.10
-------------

Console output is prefixed with `->`.
```
(A 1 10)
-> 1024

(A 2 4)
-> 65536

(A 3 3)
-> 65536
```

The function `f` computes `2n` from the definition of `A`.

To understand the function `g`, we expand our call to `A` to notice that
```
(A 1 n)
(A 0 (A 1 (- n 1)))
(A 0 (A 0 (A 1 (- n 2))))
...
```
We see that this simply gives rise to `(A 0 n)` applied recursively `n` times.
That is we get that `g` computes $`2^n`$.

Just as `g` applied `f` recursively, we can equally well conclude that `h` applies `g` recursively.
That is we get that `h` computes $`2^{2^{2^{2^{...}}}}`$ where there are `n` 2's in the power tower.
This function is usually refered to as quadrature.

Exercise 1.11
-------------

Implementation is given [here](scripts/Chapter1/exercise11.scm).

Exercise 1.12
-------------

Implementation is given [here](scripts/Chapter1/exercise12.scm).

Exercise 1.13
-------------

First we calculate a simple form for the square of $`\varphi`$ and $`\psi`$.
```math
\begin{aligned}
\varphi^2 &= \left( \frac{1 + \sqrt{5}}{2} \right)^2        \\
       &= \frac{1}{4} \left(1 + 5 + 2\sqrt{5} \right)       \\
       &= \frac{1}{4} \left(4 + 2(1 + \sqrt{5}) \right)     \\
       &= 1 + \frac{1 + \sqrt{5}}{2}                        \\
       &= 1 + \varphi.
\end{aligned}
```
Similarly, we can find that $`\psi^2 = 1 + \psi`$.

Let $`f(n) = (\varphi^n - \psi^n)/\sqrt{5}`$. We now aim to show by induction that $`\mathrm{Fib}(n) = f(n)`$.
First we show the base case for $`n = 0`$ and $`n = 1`$.
It takes only some simple manipulation to show that $`f(0) = 0 = \mathrm{Fib}(0)`$ and $`f(1) = 1 = \mathrm{Fib}(1)`$

Now we assume for some natural number $`k`$ that $`f(k) = \mathrm{Fib}(k)`$ and $`f(k + 1) = \mathrm{Fib}(k + 1)`$ and aim to show that $`f(k + 2) = \mathrm{Fib}(k + 2)`$.
We examine $`f(k + 2)`$:
```math
\begin{aligned}
f(k + 2) &= \frac{\varphi^{k + 2} - \psi^{k + 2}}{\sqrt{5}}                                         \\
         &= \frac{\varphi^k \varphi^2 - \psi^k \psi^2}{\sqrt{5}}                                    \\
         &= \frac{\varphi^k (1 + \varphi) - \psi^k (1 + \psi)}{\sqrt{5}}                            \\
         &= \frac{\varphi^k - \psi^k + \varphi^{k + 1} - \psi^{k + 1}}{\sqrt{5}}                    \\
         &= \frac{\varphi^k - \psi^k}{\sqrt{5}} + \frac{\varphi^{k + 1} - \psi^{k + 1}}{\sqrt{5}}   \\
         &= f(k) + f(k + 1)                                                                         \\
         &= \mathrm{Fib}(k) + \mathrm{Fib}(k + 1)                                                   \\
         &= \mathrm{Fib}(k + 2).
\end{aligned}
```
Invoking the principle of mathematical induction, we obtain that $`f(n) = \mathrm{Fib}(n)`$ for natural $`n`$.

Next we aim to show that $`\mathrm{Fib}(n)`$ is the closest integer to $`\varphi^n / \sqrt{5}`$.
Noting that as $`| \psi^n | < 1`$ we must have for all natural n that $`| \psi^n / \sqrt{5} | < 0.5`$ Using this fact we can compute
```math
\begin{aligned}
\left| \frac{\varphi^n}{\sqrt{5}} - \mathrm{Fib}(n) \right| &= \left| \frac{\varphi^n}{\sqrt{5}} - f(n) \right| \\
    &= \left| \frac{\varphi^n}{\sqrt{5}} - \frac{\varphi^n - \psi^n}{\sqrt{5}} \right|                          \\
    &= \left| \frac{\psi^n}{\sqrt{5}} \right|                                                                   \\
    &< 0.5.
\end{aligned}
```
This proves the assertion.

Exercise 1.14
-------------

In the following tree, I have elected for brevity to only show the paths that result in a success.
In reality, where I show only one branch from a call to `cc` there is in actuality another call that immediately returns 0.

![Call tree for (cc 11 5)](images/1-14_tree.png)

From this image we can clearly see that the longest chain is the one where we change the original amount into pennies.
This length of this chain grows linearly with the amount being changed so we have space complexity $`\Theta(n)`$.

The time complexity is perhaps more complicated than it at first appears.
While it is certainly true that the time complexity has an *upper* exponential bound, meaning the time complexity is $`O(n)`$.
It is not clear however that that such a lower exponential bound exists.
The difficulty lies in that we can only branch right at most 5 times on any given chain as their are only ever 5 types of coins.
I have a hunch that the time complexity may actually be polynomial (maybe $`\Theta(n^5)`$? I'll look into this later).

As further justification of this consider the case where we only have one type of coin.
In this case, we the call tree cannot branch at all and we clearly have linear time complexity.

Exercise 1.15
-------------

##### a.

The procedure `p` is applied 5 times when `(sine 12.5)` is evaluated.
This result can be obtained by adding a call to `display` inside the definition of `p`, but it can also be understood without resorting to experimentation.
The operand of `sine` is repeatedly divided by 3, each time calling `p`, untill it is smaller than 0.1.
We see that $`12.15 / 3^4 = 0.15 > 0.1`$ while $`12.15 / 3^5 = 0.05 < 0.1`$ so we require 5 applications of `p` to make the operand sufficently small.

##### b.
The number of steps required can be obtained by writting the operand in the form $`\theta = k 3^n`$ where $`n`$ is the smallest natural number such that $`k < 0.1`$.
In this form, the number of steps required is $`n`$ which can be calculated with a logarithm.
We can obtain that $`n = \log_3(\theta / k)`$ and thus the time complexity is $`\Theta(\log(\theta))`$.
At each step, there is exactly one deferred call to `p` so the space complexity is also $`\Theta(\log(\theta))`$

Exercise 1.16
-------------

Implementation is given [here](scripts/Chapter1/exercise16.scm).

Exercise 1.17
-------------

Implementation is given [here](scripts/Chapter1/exercise17.scm).

Exercise 1.18
-------------

Implementation is given [here](scripts/Chapter1/exercise18.scm).

Exercise 1.19
-------------

The new transformation $`T_{p' q'}`$ can be calculated as
```math
\begin{aligned}
p' &= p^2 + q^2 \\
q' &= 2pq + q^2
\end{aligned}
```

The implementation is given [here](scripts/Chapter1/exercise19.scm).

Exercise 1.20
-------------

Console output is prefixed with `->`.

We first examine a call to `(gcd 206 40)` using normal-order evaluation.
```
(gcd 206
     40)

(gcd 40
     (remainder 206 40))

; 1 call to remainder evaluating the if predicate
(gcd (remainder 206 40)
     (remainder 40 (remainder 206 40)))

; 2 calls to remainder evaluating the if predicate
(gcd (remainder 40 (remainder 206 40))
     (remainder (remainder 206 40) (remainder 40 (remainder 206 40))))

; 4 calls to remainder evaluating the if predicate
(gcd (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))
     (remainder (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40))))

; 7 calls to remainder evaluating the if predicate
(remainder (remainder 206 40) (remainder 40 (remainder 206 40))) ; 4 calls to remainder

-> 2
```
This gives a total of 18 calls to `remainder` when using normal-order evaluation.

Now we examine a call to `(gcd 206 40)` using applicative-order.
```
(gcd 206 40)

(gcd 40 (remainder 206 40)) ; 1 call to remainder
(gcd 40 6)

(gcd 6 (remainder 40 6)) ; 1 call to remainder
(gcd 6 4)

(gcd 4 (remainder 6 4)) ; 1 call to remainder
(gcd 4 2)

(gcd 2 (remainder 4 2)) ; 1 call to remainder
(gcd 2 0)

-> 2
```
This gives a total of 4 calls to `remainder` when using applicative-order evaluation.

Exercise 1.21
-------------

Console output is prefixed with `->`.
```
(smallest-divisor 199)
-> 199

(smallest-divisor 1999)
-> 1999

(smallest-divisor 19999)
-> 7
```

Exercise 1.22
-------------

The following data is obtained by the `search-for-primes` procedure, see [here](scripts/Chapter1/exercise22.scm) for an implementation.
```
1009 *** 4492
1013 *** 6877
1019 *** 4514
average: 5294

10007 *** 13566
10009 *** 13378
10037 *** 13654
average:  13533

100003 *** 55960
100019 *** 56866
100043 *** 49810
average:   54212

1000003 *** 146227
1000033 *** 151341
1000037 *** 151410
average:    149659
```

The average time for primes greater than 10,000 increased from the time for primes greater than 1000 by a factor of 2.56.
The average time for primes greater than 100,000 increased from the time for primes greater than 10,000 by a factor of 4.01.
The average time for primes greater than 1,000,000 increased from the time for primes greater than 100,000 by a factor of 2.76.

The expected factor for all these increases is $`\sqrt{10} = 3.16`.
Our numbers are pretty close to this so it does indeed appear that this primality test is $`\Theta(\sqrt{n})`$.

Exercise 1.23
-------------

The following is the output of the `run-test` procedure with averages appended, see [here](scripts/Chapter1/exercise23.scm) for an implementation.
```
1009 *** 4519
1013 *** 3347
1019 *** 3364
average: 3743

10007 *** 13348
10009 *** 11640
10037 *** 13008
average:  12665

100003 *** 41746
100019 *** 47385
100043 *** 48985
average:   46038

1000003 *** 104795
1000033 *** 101806
1000037 *** 92554
average:    99718
```

Although these times are certainly faster than the previous test, but it is a far cry from the twice as fast we would expect.
It is certainly true that we check half as many factors, but we also introduce an additional `if` special form whose predicate needs to be evaluated.
So we have decreased the number of steps, but increased how long each step takes.

Exercise 1.24
-------------

The following is the output of the `(run-test 5)` procedure with averages appended, see [here](scripts/Chapter1/exercise24.scm) for an implementation.
```
1009 *** 23063
1013 *** 18748
1019 *** 24724
average: 22178

10007 *** 33011
10009 *** 25489
10037 *** 34043
average:  30848

100003 *** 28341
100019 *** 36517
100043 *** 28426
average:   31096

1000003 *** 37171
1000033 *** 32004
1000037 *** 31916
average:    33697
```

As $`10^6 = \left(10^3 \right)^2`$ we would expect the times for primes near 1,000,000 to take roughly twice as long to test as the primes near 1000.
The actual factor is much closer to 1.5. I suspect that these tests use numbers that are too small to properly observe the logarithmic asymptotic nature of this test.

Exercise 1.25
-------------

When dealing with large numbers (larger than a single word, on modern computers this means larger than $`2^64`$), the speed of operations is related to the size of the numbers.
By computing `expmod` using a call to `exp` we first compute a very large number as the result of `exp` and pass that to `remainder`.
The fact that we have to manipulate such large numbers slows the computation down significantly.

On the other hand, by exploiting the fact that we can apply the `remainder` function as we are calculating `expmod`, we can keep the numbers we work with smaller than the candidate prime.
This makes the overall execution much faster.

So no, Alyssa P. Hacker isn't correct in assuming that her `expmod` works just as well.
Although it will produce correct results it will take a much longer time to produce results.

Exercise 1.26
-------------

By using a call to `*` instead of `square` we end up calculating `(exp base (/ exp 2) m)` twice, the interpreter can't tell that it is calculating the same thing over again.
At each step we now *branch* resulting in an exponential component to the time complexity.
As a result our complexity becomes $`\Theta \left(\log e^n \right) = \Theta(n)`$.

If we were to use `square` instead, we only calculate `(exp base (/ exp 2) m)` once, and then multiply the result with itself.
If Louis didn't want to explicitly define a `square` function, he should of used a lambda.

Exercise 1.27
-------------

Implementation is given [here](scripts/Chapter1/exercise27.scm).
The result of running `(run-tests)` is
```
561	    pass
1105    pass
1729    pass
2465    pass
2821    pass
6601    pass
```

This indicates that these numbers do indeed "fool" the Fermat test.

Exercise 1.28
-------------

See [here](scripts/Chapter1/exercise28.scm) for an implementation of the Miller-Rabin test.
The results of for a pair of twin primes near 10,000 and the Carmichael numbers.
All primes pass the test, and all non-primes including the Carmichael numbers fail the test despite only running the test once for each number.
```
10005
10006
10007	pass
10008
10009	pass
10010
10011

561
1105
1729
2465
2821
6601
```

Exercise 1.29
-------------

See [here](scripts/Chapter1/exercise29.scm) for an implementation of integration using Simpson's rule.
The results of some integrations of `cube` with ouput prefixed with `->` are shown below.
```
(simpson-integration cube 0 1 100)
-> 0.25

(simpson-integration cube 0 1 1000)
-> 0.25
```

Both values of `n` produce the exact result of the integral (It turns out Simpson's rule produces exact results for any polynomial of order 3 or less) which is clearly an improvement.

Exercise 1.30
-------------

Implementation is given [here](scripts/Chapter1/exercise30.scm).

Exercise 1.31
-------------

Implementation is given [here](scripts/Chapter1/exercise31.scm).

Here are some aproximations to $`\pi`$ using the given formula, output is prefixed with `->`.
```
(pi-prod 100)
-> 3.157339689217561

(pi-prod 10000)
-> 3.141749737149238

(pi-prod 1000000)
-> 3.141594224386107
```

Exercise 1.32
-------------

Implementation is given [here](scripts/Chapter1/exercise32.scm).

Exercise 1.33
-------------

Implementation is given [here](scripts/Chapter1/exercise33.scm).

Here is some example output for calculating sums of squares of primes; output is prefixed with `->`.
```
(sum-prime-squares 2 10000)
-> 37546387960

(sum-prime-squares 1000 2000)
-> 310367391

(sum-prime-squares 10000 11000)
-> 11678398882
```

Here is some example output for calculating the products of coprimes; output is prefixed with `->`.
```
(coprime-prod 36)
-> 29248404810625

(coprime-prod 120)
-> 43050594261939447419527053602904653655332696392323361

(coprime-prod 360)
-> 51518848095250197764892806036331166962037493506141919518314220368911169465677
   25841326056915179040353839732529390314037242746722186920911771047936178399661
   66001196537587406893845709147918133356078837718881
```

Exercise 1.34
-------------

Let us examine the evaluation order, this applies to both applicative and normal-order.
```
(f f)
(f 2)
(2 2)
```
Which then gives an error as it tries to call the number `2` as if it were a function.

Exercise 1.35
-------------

First, let use define the golden ratio as $`\varphi := \left( 1 + \sqrt(5) \right) / 2`$.
Next let us define a function $`f`$ by the rule $`f(x) = 1 + 1/x`$.
Now we computer
```math
\begin{aligned}
f(\varphi) &= 1 + \frac{1}{\varphi}                                                     \\
           &= 1 + \frac{1}{\frac{1 + \sqrt{5}}{2}}                                      \\
           &= 1 + \frac{2}{1 + \sqrt{5}}                                                \\
           &= \frac{3 + \sqrt{5}}{1 + \sqrt{5}}                                         \\
           &= \frac{3 + \sqrt{5}}{1 + \sqrt{5}} \cdot \frac{1 - \sqrt{5}}{1 - \sqrt{5}} \\
           &= \frac{-2 - 2\sqrt{5}}{-4}                                                 \\
           &= \frac{1 + \sqrt{5}}{2}                                                    \\
           &= \varphi.
\end{aligned}
```
Thus $`\varphi`$ is a fixed point of $`f`$.

With the implementation of `fixed-point` given, we obtain
```
(fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.5)
-> 1.6180327868852458
```

Exercise 1.36
-------------

See [here](scripts/Chapter1/exercise36.scm) for an implementation.
For the undamped function, we can find the fixed point of `x^x = 1000` with
```
(log-fixed-point (lambda (x) (/ (log 1000) (log x))) 4.5)
-> 0:      4.5
   1:      4.592686050457043
   2:      4.5312651038512355
   3:      4.571641242223737
   4:      4.544957754239433
   5:      4.562530501803623
   6:      4.550930964390589
   7:      4.558576009340466
   8:      4.5535322316030475
   9:      4.556857632751284
   10:     4.554664211855043
   11:     4.55611056630435
   12:     4.5551566504814
   13:     4.555785708825408
   14:     4.555370842956472
   15:     4.555644433327481
   16:     4.555464003005006
   17:     4.555582992314503
   18:     4.55550452059762
   19:     4.555556271018502
   20:     4.555522142489372
   21:     4.555544649577365
   22:     4.555529806555377
   23:     4.555539595243813
```

Using average damping to speed up convergence gives
```
(log-fixed-point (damp (lambda (x) (/ (log 1000) (log x)))) 4.5)
-> 0:      4.5
   1:      4.546343025228522
   2:      4.553977675530787
   3:      4.5552706399207485
   4:      4.55549058125609
   5:      4.555528022594382
   6:      4.555534397165625
```

The difference in the number of steps required is rather stark showcasing the benifit of average damping quite well.

Exercise 1.37
-------------

See [here](scripts/Chapter1/exercise37.scm) for both a recursive and iterative implementation of `cont-frac`.

Here is the result of running the approximation up to 15 for the value of `k`.
```
1:      1.0
2:      0.5
3:      0.6666666666666666
4:      0.6000000000000001
5:      0.625
6:      0.6153846153846154
7:      0.6190476190476191
8:      0.6176470588235294
9:      0.6181818181818182
10:     0.6179775280898876
11:     0.6180555555555556
12:     0.6180257510729613
13:     0.6180371352785146
14:     0.6180327868852459
15:     0.6180344478216819
```
The value we are looking for is $`1/\varphi = 0.6180339887498948`$ so we get an answer accurate to 4 decimal places with `k` at least 12.

Exercise 1.38
-------------

Implementation is given [here](scripts/Chapter1/exercise38.scm).

Exercise 1.39
-------------

Implementation is given [here](scripts/Chapter1/exercise39.scm).

Exercise 1.40
-------------

Implementation is given [here](scripts/Chapter1/exercise40.scm).

Exercise 1.41
-------------

See [here](scripts/Chapter1/exercise41.scm) for an implementation of `double`.
With the function defined as such we have
```
(((double (double double)) inc) 5)
-> 21
```

Exercise 1.42
-------------

Implementation is given [here](scripts/Chapter1/exercise42.scm).

Exercise 1.43
-------------

Implementation is given [here](scripts/Chapter1/exercise43.scm).

Exercise 1.44
-------------

Implementation is given [here](scripts/Chapter1/exercise44.scm).

We can obtain n-fold smoothed functions as `((repeated smooth n) f)`

Exercise 1.45
-------------

Implementations are given [here](scripts/Chatper1/exercise45.scm).

First, I implemented a function `(root-explicit-damps n x damps)` that can have the number of average damps explicitly passed as a parameter.
With this I experiemented to find the minimum number of damps needed to succesfully converge, the results are as follows

| $`n`$-th root | minimum average damps |
|---------------|-----------------------|
| 2             | 1                     |
| 3             | 1                     |
| 4             | 2                     |
| 5             | 2                     |
| 6             | 2                     |
| 7             | 2                     |
| 8             | 3                     |
| 15            | 3                     |
| 16            | 4                     |
| 31            | 4                     |
| 32            | 5                     |

We see that the minimum number of damps required to calculate an $`n`$th root is $`\lfloor log_2 (n) \rfloor`$.
Using this hypothesis (I really want a proof for this...) I have implemented the `(root n x)` function in the file linked above.

Exercise 1.46
-------------

Implementation is given [here](scripts/Chapter1/exercise46.scm).
