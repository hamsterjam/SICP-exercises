SICP Exercises
==============

This is a documentation of my answers to the exercises in the fantastic book 'Structure and Interpretation of Computer Programs'.
The book uses scheme, and I specifically use GNU guile as my scheme implementation.

The exercises to each chapter are recorded in their own markdown folders and exercises that require writting actual code usually have their own file in the scripts folder.
You can find my answers here

[Chapter 1](Chapter1.md)

[Chapter 2](Chapter2.md)
